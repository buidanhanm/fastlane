# Fastlane 

The easiest way to build and release mobile apps.  

Link tham khảo  : https://fastlane.tools/ 

#### 1:Yêu cầu  
Để chạy fastlane yêu cầu máy tính cần cài đặt các thư viện dưới đây : 

Python       :  https://www.python.org/ 

Firebase CLI : https://firebase.google.com/docs/cli#install_the_firebase_cli 

#### 2:Cài đặt  

+ Cài đặt Fastlne 
```
# Using RubyGems
sudo gem install fastlane -NV

```

+ Cài đặt  plugin `firebase_app_distribution`

 ```
 fastlane add_plugin firebase_app_distribution
 ```

+ Cài đặt  plugin `telegram`

 ```
 fastlane add_plugin telegram
 ```
### 3:Tạo Product Flavors cho Android ( Bắt buộc ) 
 Tạo 2 Product Flavors với 2 tên : 
  
  + `develop` : cho môi trường Develop
  
  + `production` : cho môi trường Production
  
  Tham khảo : https://developer.android.com/studio/build/build-variants
 ### 4:Tạo Scheme cho iOS ( Bắt buộc ) 
 Tạo 2 Scheme với 2 tên : 
  
  + `SCHEME_NAME_IOS`-develop : cho môi trường Develop
  
  + `SCHEME_NAME_IOS` : cho môi trường Production
  
  Lưu ý : Tên của `SCHEME_NAME_IOS` được lấy từ `SCHEME_NAME_IOS` trong file `fastlane/.env.secret`
  
  VD: `SCHEME_NAME_IOS` là "MYVNPT" thì sẽ tạo ra 2 Scheme MYVNPT-develop và MYVNPT
  
  Tham khảo : https://stackoverflow.com/questions/16416092/how-to-change-the-name-of-the-active-scheme-in-xcode
  
#### 5: Bổ sung thông tin key

+ Copy thư mục `fastlane` vào trong thư mục gốc của dự án
+ Trong file  `fastlane/.env.secret` bổ sung thông tin của theo bảng dưới đây  : 

| KEY | Ý nghĩa |  Tham khảo |
| -------- | -------- | -------- |
| `ANDROID_APP_DEV ANDROID_APP_PROD `     |  App ID của ứng dụng Android trên Firebase App ID tương ứng với môi trường Develop và Production |  [APP ID ](https://console.firebase.google.com/u/0/project/_/settings/general/)     |
| `IDENTIFIER_IOS_APP_DEV IDENTIFIER_IOS_APP_PROD`     | 	Bundle Identifiers của ứng dụng iOS tương ứng với môi trường Develop và Production  | [Bundle](https://developer.apple.com/documentation/appstoreconnectapi/bundle_ids)     |
| `SCHEME_NAME_IOS`     | Tên SCHEME của ứng dụng iOS  |      |
| `FASTLANE_USER FASTLANE_PASSWORD`     | Username và Password của tài khoản Apple Developer   |  [Account apple](https://docs.fastlane.tools/best-practices/continuous-integration/#environment-variables-to-set)    |
| `FASTLANE_APPLE_APPLICATION_SPECIFIC_PASSWORD`     |  Đối với tài khoản Apple Developer thì cần bổ sung thêm thông tin SPECIFIC_PASSWORD để có thẻ access vào tài khoản   | [SPECIFIC_PASSWORD](https://docs.fastlane.tools/best-practices/continuous-integration/#application-specific-passwords)     |
| `GROUP_TESTER_DEV GROUP_TESTER_PROD`     | Tên alias của group tester trên firebase ( đối với ứng dụng Android )  | [Manage Testers ](https://firebase.google.com/docs/app-distribution/manage-testers)     |
|`TELEGRAM_ID_CHAT`      | ID của group Telegram  |   [ID Group Telegram ](https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id)|
|`TELEGRAM_BOT_TOKEN`      | Token của BOT Telegram |   [BOT Telegram ](https://core.telegram.org/bots)|
|`TELEGRAM_MEMBER`      | Danh sách các member cần mention, sử dụng @username hoặc @channel để mention all member trong group |  |

#### 6:Chạy fastlane 
Trong thư mục gốc của dự án chạy command : 

```
fastlane 
```

Trong terminal sẽ hiển thị bảng với những lane được định nghĩ trước .Chon số theo thứ tự của lane trong bảng đó.

Mô tả lane : 

| Lane | Ý nghĩa  | 
| -------- | -------- | 
| inputNote  | Lane để nhâp nội dung note cho bản build , thông tin được nhâp sẽ được lưu vào file `./temp/AndroidReleaseNote.txt` cho Android và `./temp/iOSReleaseNote.txt` cho iOS . Kêt thúc bằng khi gõ `END`  | 
| ios Develop , ios Production  | Lane để chạy 4 lane theo thứ tự `inputNote` `ios Build_Develop` ` ios TestFlight_Develop`  ` ios Notify_Telegram`  | 
| ios Build_Develop ios Build_Production | Lane này để build iOS và generate ra file `ipa` theo môi trường Develop hoặc Production  | 
| ios TestFlight_Develop ios TestFlight_Production | Lane này để đẩy file `ipa` được generate ở bên trên lên TestFlight  | 
| android Develop  android Production  |  Lane để chạy 4 lane theo thứ tự `inputNote` `android Build_Develop` ` android Distribute_Develop`  ` android Notify_Telegram` | 
| android Build_Develop android Build_Production | Lane này để build Android  và generate ra file `apk` theo môi trường Develop hoặc Production  | 
| android Distribute_Develop android Distribute_Production |  Lane này để đẩy file `apk` được generate ở bên trên lên Firebase  | 
|  android Notify_Telegram   | Lane này để thông báo một tin nhắn lên group Telegram qua BOT được tạo ở trên | 
