# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane


default_platform(:ios)

before_all do
	Dotenv.overload '.env.secret'
end

after_all do |lane|

end

lane :inputNote do |options|
	releaseNoteInput = prompt(
    text: "Please enter your release notes. This note will be pushed on Firebase: ",
    multi_line_end_keyword: "END"
    )
  time1 = Time.new
  time = time1.strftime("%Y-%m-%d %H:%M:%S")
  releaseNote =  releaseNoteInput
   #save to file
   commit = last_git_commit

   File.write("./temp/content.txt", releaseNote)
   contentSave =
   "
   ---------------------------------------------------------------
     New build version has been uploaded successfully to Firebase!
     Time : #{time}
     Platform : #{options[:platform]}
     Environment : #{options[:buildType]}
     Author : #{commit[:author]}
     Author email : #{commit[:author_email]}
     Note : #{releaseNote}
   ---------------------------------------------------------------
   "
   if(options[:platform] === 'iOS') 
    File.write("./temp/AndroidReleaseNote.txt", contentSave)
   else 
    File.write("./temp/iOSReleaseNote.txt", contentSave)
   end
   
end
  
 
#++++++++++++++++++++

platform :ios do

#++++++++++++++++++++

lane :Develop do |options|
  # input note 
  inputNote(platform: 'iOS', buildType:'Develop')
  #build
  begin
  	Build_Develop()
  rescue => ex
  	UI.error(ex)
  	next
  end
   # Distribute
  begin
    	TestFlight_Develop()
  rescue => ex
    	UI.error(ex)
    	next
    end
   # Notify
  begin
      Notify_Telegram()
  rescue => ex
      UI.error(ex)
      next
    end
end

lane :Production do |options|
  
  inputNote(platform: 'iOS', buildType:'Production')
  #build
  begin
  	Build_Production()
  rescue => ex
  	UI.error(ex)
  	next
  end
    #Distribute
    begin
    	TestFlight_Production()
    rescue => ex
    	UI.error(ex)
    	next
    end
  begin
      Notify_Telegram()
  rescue => ex
      UI.error(ex)
      next
    end
end

#++++++++++++++++++++
lane :Build_Develop do
    #build
    gym(
      scheme: "#{ENV['SCHEME_NAME_IOS']}-dev",
      workspace: "./ios/#{ENV['SCHEME_NAME_IOS']}.xcworkspace",
      output_name: "#{ENV['SCHEME_NAME_IOS']}.ipa",
      #clean: true
      )
end
#++++++++++++++++++++


#++++++++++++++++++++

lane :Build_Production do
    #build
    gym(
      scheme: "#{ENV['SCHEME_NAME_IOS']}",
      workspace: "./ios/#{ENV['SCHEME_NAME_IOS']}.xcworkspace",
      output_name: "#{ENV['SCHEME_NAME_IOS']}.ipa",
      #clean: true
      )
end
#++++++++++++++++++++

lane :TestFlight_Develop do 
  upload_to_testflight(
    skip_waiting_for_build_processing: true,
    app_identifier: "#{ENV['IDENTIFIER_IOS_APP_DEV']}" ,
    ipa:"#{ENV['SCHEME_NAME_IOS']}.ipa",
    )
end

lane :TestFlight_Production do 
  upload_to_testflight(
    skip_waiting_for_build_processing: true,
    app_identifier:"#{ENV['IDENTIFIER_IOS_APP_PROD']}" ,
    ipa:"#{ENV['SCHEME_NAME_IOS']}.ipa",
    testers_file_path: "./tester_ios.csv"
    )
end

#++++++++++++++++++
lane :Notify_Telegram do |options|
    content = File.read("./temp/iOSReleaseNote.txt")

    telegram(
      token: "#{ENV['TELEGRAM_BOT_TOKEN']}",
      chat_id: ENV['TELEGRAM_ID_CHAT'],
      text: "#{ENV['TELEGRAM_MEMBER']} #{content}",
    )
 end

end




platform :android do
lane :Develop do |options| 

 inputNote(platform: 'Android', buildType:'Develop')
  #build
  begin
    Build_Develop()
  rescue => ex
    UI.error(ex)
    next
  end
    #Distribute
    begin
      Distribute_Develop()
    rescue => ex
      UI.error(ex)
      next
    end
  begin
      Notify_Telegram()
  rescue => ex
      UI.error(ex)
      next
    end
end

lane :Production do |options| 

 #inputNote(platform: 'Android', buildType:'Production')
  
  begin
    Build_Production()
  rescue => ex
    UI.error(ex)
    next
  end

  begin
      Distribute_Production()
  rescue => ex
      UI.error(ex)
      next
  end
  begin
      Notify_Telegram()
  rescue => ex
      UI.error(ex)
      next
    end
end

#++++++++++++++++++++
lane :Build_Develop do 
   	gradle(task: 'clean', project_dir: 'android/')
    gradle(task: 'assemble', build_type: 'developRelease' ,project_dir: 'android/')
end

#++++++++++++++++++++

lane :Build_Production do
    gradle(task: 'clean', project_dir: 'android/')
    gradle(task: 'assemble', build_type: 'productionRelease',project_dir: 'android/')
end

#++++++++++++++++++++

lane :Distribute_Develop do
    releaseNote = File.read("./temp/content.txt")
	puts "------------------- Starting distribution via Fabric  .......... "
	va = File.absolute_path("../android/app/build/outputs/apk/")
	va2 = va+'/*/*/*.apk'
	path = Dir[va2][0]
	puts "------------------- APK .......... #{path}  -------"
	
          firebase_app_distribution(
              app:"#{ENV['ANDROID_APP_DEV']}" ,
               apk_path: path,
               release_notes: "#{releaseNote}",
               groups:"#{ENV['GROUP_TESTER_DEV']}"
          )
end

lane :Distribute_Production do
    releaseNote = File.read("./temp/content.txt")
 
  va = File.absolute_path("../android/app/build/outputs/apk/")
  va2 = va+'/*/*/*.apk'
  path = Dir[va2][0]
  puts "------------------- APK .......... #{path}  -------"
          firebase_app_distribution(
              app:"#{ENV['ANDROID_APP_PROD']}" ,
               apk_path: path,
               release_notes: "#{releaseNote}",
               groups:"#{ENV['GROUP_TESTER_PROD']}"
          )
end

#+++++++++++++++++++++++++
lane :Notify_Telegram do |options|
    content = File.read("./temp/AndroidReleaseNote.txt")

    telegram(
      token: "#{ENV['TELEGRAM_BOT_TOKEN']}",
      chat_id: "#{ENV['ID_TELEGRAM_CHAT']}",
      text: "#{ENV['TELEGRAM_MEMBER']} #{content}",
    )
 end
end